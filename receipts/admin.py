from django.contrib import admin

from receipts.models import Receipt, Account, ExpenseCategory


class Receiptadmin(admin.ModelAdmin):
    pass


class Accountadmin(admin.ModelAdmin):
    pass


class ExpenseCategoryadmin(admin.ModelAdmin):
    pass


admin.site.register(Receipt, Receiptadmin)
admin.site.register(Account, Accountadmin)
admin.site.register(ExpenseCategory, ExpenseCategoryadmin)
